package com.napp.composemigration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.napp.composemigration.data.Item
import com.napp.composemigration.data.Dummy

class MainViewModel : ViewModel() {
    var itemList: MutableLiveData<List<Item>> = MutableLiveData<List<Item>>().apply {
        value = Dummy.ITEM_LIST
    }
}