package com.napp.composemigration.ui.compose

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.napp.composemigration.data.Item
import com.napp.composemigration.data.Dummy

//https://developer.android.com/jetpack/compose/interop

@Composable
fun ComposeItemListLayout(list: List<Item>) {
    Text(text = "Hello Bob!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeItemListLayout(Dummy.ITEM_LIST)
}

//// Link compose container with
//binding.listComposeView.setContent {
//    ComposeMigrationTheme {
//        ComposeItemListLayout(it)
//    }
//    ComposeItemListLayout(it)
//}

//<androidx.compose.ui.platform.ComposeView
//android:id="@+id/listComposeView"
//android:layout_width="match_parent"
//android:layout_height="0dp"
//android:layout_weight="1"/>