package com.napp.composemigration.ui.xml

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.napp.composemigration.databinding.LayoutXmlItemBinding

class XmlItemLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = LayoutXmlItemBinding.inflate(
            LayoutInflater.from(context), this
    )

    init {
        updateAccessibility()
    }

    var primaryText: CharSequence
        get() = binding.primaryTv.text
        set(value) {
            binding.primaryTv.text = value
        }

    var secondaryText: CharSequence
        get() = binding.secondaryTv.text
        set(value) {
            binding.secondaryTv.text = value
        }

    fun setSecondaryText(@StringRes resId: Int) {
        binding.secondaryTv.setText(resId)
    }

    var amountText: CharSequence
        get() = binding.amountTv.text
        set(value) {
            binding.amountTv.text = value
        }

    var iconVisibility: Int
        get() = binding.iconIV.visibility
        set(value) {
            binding.iconIV.visibility = value
        }

    private fun updateAccessibility() {
        contentDescription = "$primaryText. $secondaryText. $amountText"
    }
}


