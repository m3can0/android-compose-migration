package com.napp.composemigration.ui.xml

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.napp.composemigration.R
import com.napp.composemigration.data.Item
import com.napp.composemigration.databinding.ItemXmlBinding

class XmlItemAdapter(
    private val list: List<Item>,
    private val listener: Listener
) : RecyclerView.Adapter<XmlItemAdapter.XmlItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): XmlItemHolder {
        val binding = ItemXmlBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return XmlItemHolder(binding)
    }

    override fun onBindViewHolder(holder: XmlItemHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    inner class XmlItemHolder(private val binding: ItemXmlBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Item) {
            with(item) {
                binding.root.primaryText = name
                binding.root.setSecondaryText(category)
                binding.root.amountText = itemView.context.getString(R.string.price, amount)

                if(item.isClickEnabled) {
                    binding.root.iconVisibility = View.VISIBLE
                    binding.root.setOnClickListener {
                        listener.onItemClick(item)
                    }
                }
            }
        }
    }

    interface Listener {
        fun onItemClick(item: Item)
    }
}