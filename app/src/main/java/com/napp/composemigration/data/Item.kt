package com.napp.composemigration.data

import androidx.annotation.StringRes
import com.napp.composemigration.R

data class Item(
    val id: Int,
    @StringRes val category: Int,
    val amount: Double,
    val isClickEnabled: Boolean = true,
    val name: String = "Item $id"
)

object Dummy {
    val ITEM_LIST: List<Item> = arrayListOf(
        Item(1, R.string.category_shirt, 20.0, isClickEnabled = false),
        Item(2, R.string.category_hoodies, 42.0, isClickEnabled = false),
        Item(3, R.string.category_shoes, 10.0, isClickEnabled = false),
        Item(4, R.string.category_bra, 103.0),
        Item(5, R.string.category_watch, 500.0),
        Item(6, R.string.category_shirt, 35.0),
        Item(7, R.string.category_hoodies, 65.0),
        Item(8, R.string.category_shoes, 85.0),
        Item(9, R.string.category_bra, 32.0),
        Item(10, R.string.category_watch, 86.0)
    )
}
