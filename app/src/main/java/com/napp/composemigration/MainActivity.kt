package com.napp.composemigration

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.napp.composemigration.data.Item
import com.napp.composemigration.databinding.ActivityMainBinding
import com.napp.composemigration.ui.xml.XmlItemAdapter

class MainActivity : AppCompatActivity(), XmlItemAdapter.Listener{

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        viewModel.itemList.observe(this) {
            // Set adapter
            binding.xmlRecycler.apply {
                adapter = XmlItemAdapter(it, this@MainActivity)
            }
        }
    }

    override fun onItemClick(item: Item) {
        Toast.makeText(this, "${item.name} clicked!!!", Toast.LENGTH_SHORT).show()
    }
}